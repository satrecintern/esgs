﻿namespace PosAttDataGenerator
{
    partial class PosAttDataGenerator
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.time_textbox = new System.Windows.Forms.TextBox();
            this.interval_textbox = new System.Windows.Forms.TextBox();
            this.time_label = new System.Windows.Forms.Label();
            this.interval_label = new System.Windows.Forms.Label();
            this.transfer_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.inputFileTextBox = new System.Windows.Forms.TextBox();
            this.selectInputFile = new System.Windows.Forms.Button();
            this.selectOutputFolder = new System.Windows.Forms.Button();
            this.outputFolderTextBox = new System.Windows.Forms.TextBox();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // time_textbox
            // 
            this.time_textbox.Location = new System.Drawing.Point(117, 75);
            this.time_textbox.Name = "time_textbox";
            this.time_textbox.Size = new System.Drawing.Size(231, 21);
            this.time_textbox.TabIndex = 2;
            // 
            // interval_textbox
            // 
            this.interval_textbox.Location = new System.Drawing.Point(404, 75);
            this.interval_textbox.Name = "interval_textbox";
            this.interval_textbox.Size = new System.Drawing.Size(73, 21);
            this.interval_textbox.TabIndex = 3;
            this.interval_textbox.Text = "8";
            // 
            // time_label
            // 
            this.time_label.AutoSize = true;
            this.time_label.Location = new System.Drawing.Point(7, 80);
            this.time_label.Name = "time_label";
            this.time_label.Size = new System.Drawing.Size(108, 12);
            this.time_label.TabIndex = 9;
            this.time_label.Text = "Data Start Time :  ";
            // 
            // interval_label
            // 
            this.interval_label.AutoSize = true;
            this.interval_label.Location = new System.Drawing.Point(364, 80);
            this.interval_label.Name = "interval_label";
            this.interval_label.Size = new System.Drawing.Size(38, 12);
            this.interval_label.TabIndex = 10;
            this.interval_label.Text = "Step :";
            // 
            // transfer_button
            // 
            this.transfer_button.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.transfer_button.Location = new System.Drawing.Point(519, 75);
            this.transfer_button.Name = "transfer_button";
            this.transfer_button.Size = new System.Drawing.Size(75, 23);
            this.transfer_button.TabIndex = 12;
            this.transfer_button.Text = "Generate";
            this.transfer_button.UseVisualStyleBackColor = true;
            this.transfer_button.Click += new System.EventHandler(this.transfer_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(483, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "Hz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 12);
            this.label2.TabIndex = 14;
            this.label2.Text = "Input file :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "Output folder :";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.logTextBox);
            this.groupBox1.Controls.Add(this.selectOutputFolder);
            this.groupBox1.Controls.Add(this.outputFolderTextBox);
            this.groupBox1.Controls.Add(this.selectInputFile);
            this.groupBox1.Controls.Add(this.inputFileTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.transfer_button);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.interval_label);
            this.groupBox1.Controls.Add(this.time_label);
            this.groupBox1.Controls.Add(this.time_textbox);
            this.groupBox1.Controls.Add(this.interval_textbox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1048, 326);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // inputFileTextBox
            // 
            this.inputFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputFileTextBox.Location = new System.Drawing.Point(117, 17);
            this.inputFileTextBox.Name = "inputFileTextBox";
            this.inputFileTextBox.Size = new System.Drawing.Size(830, 21);
            this.inputFileTextBox.TabIndex = 16;
            // 
            // selectInputFile
            // 
            this.selectInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectInputFile.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.selectInputFile.Location = new System.Drawing.Point(953, 16);
            this.selectInputFile.Name = "selectInputFile";
            this.selectInputFile.Size = new System.Drawing.Size(75, 23);
            this.selectInputFile.TabIndex = 17;
            this.selectInputFile.Text = "Select";
            this.selectInputFile.UseVisualStyleBackColor = true;
            this.selectInputFile.Click += new System.EventHandler(this.selectInputFile_Click);
            // 
            // selectOutputFolder
            // 
            this.selectOutputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectOutputFolder.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.selectOutputFolder.Location = new System.Drawing.Point(953, 45);
            this.selectOutputFolder.Name = "selectOutputFolder";
            this.selectOutputFolder.Size = new System.Drawing.Size(75, 23);
            this.selectOutputFolder.TabIndex = 19;
            this.selectOutputFolder.Text = "Select";
            this.selectOutputFolder.UseVisualStyleBackColor = true;
            this.selectOutputFolder.Click += new System.EventHandler(this.selectOutputFolder_Click);
            // 
            // outputFolderTextBox
            // 
            this.outputFolderTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputFolderTextBox.Location = new System.Drawing.Point(117, 46);
            this.outputFolderTextBox.Name = "outputFolderTextBox";
            this.outputFolderTextBox.Size = new System.Drawing.Size(830, 21);
            this.outputFolderTextBox.TabIndex = 18;
            // 
            // logTextBox
            // 
            this.logTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTextBox.Location = new System.Drawing.Point(9, 102);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTextBox.Size = new System.Drawing.Size(1019, 218);
            this.logTextBox.TabIndex = 20;
            // 
            // PosAttDataGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 348);
            this.Controls.Add(this.groupBox1);
            this.Name = "PosAttDataGenerator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PosAttDataGenerator";
            this.Load += new System.EventHandler(this.PosAttDataGenerator_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox time_textbox;
        private System.Windows.Forms.TextBox interval_textbox;
        private System.Windows.Forms.Label time_label;
        private System.Windows.Forms.Label interval_label;
        private System.Windows.Forms.Button transfer_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox inputFileTextBox;
        private System.Windows.Forms.Button selectOutputFolder;
        private System.Windows.Forms.TextBox outputFolderTextBox;
        private System.Windows.Forms.Button selectInputFile;
        private System.Windows.Forms.TextBox logTextBox;
    }
}

