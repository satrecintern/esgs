﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PosAttDataGenerator
{
    public class FolderSelectDialog
    {
        #region variables
        // Wrapped dialog
        /// <summary>
        /// Wrapped dialog
        /// </summary>
        private OpenFileDialog m_ofdWrappedDialog = null;
        // Version number of vista
        /// <summary>
        /// Version number of vista
        /// </summary>
        private const int VISTA_OS_VERSION = 6;

        private const string WINDOWS_FORM_NAMESPACE = "System.Windows.Forms";
        #endregion variables

        ///
        /// <summary>
        /// 기본 생성자
        /// </summary>
        /// <author>KyungChul Shin (kcshin@satreci.com)</author>
        /// <date>2014-04-21</date>
        public FolderSelectDialog()
        {
            m_ofdWrappedDialog = new System.Windows.Forms.OpenFileDialog();

            m_ofdWrappedDialog.Filter = "Folders|\n";
            m_ofdWrappedDialog.AddExtension = false;
            m_ofdWrappedDialog.CheckFileExists = false;
            m_ofdWrappedDialog.DereferenceLinks = true;
            m_ofdWrappedDialog.Multiselect = false;
        }

        #region Properties

        /// <summary>
        /// 다이얼로그에서 보여지는 초기 폴더 경로.
        /// </summary>
        public string InitialDirectory
        {
            get { return m_ofdWrappedDialog.InitialDirectory; }
            set { m_ofdWrappedDialog.InitialDirectory = value == null || value.Length == 0 ? Environment.CurrentDirectory : value; }
        }

        /// <summary>
        /// 다이얼로그 상단에 보여지는 폴더 선택을 위한 설명 정보.
        /// </summary>
        public string Description
        {
            get { return m_ofdWrappedDialog.Title; }
            set { m_ofdWrappedDialog.Title = value == null ? "Select a folder" : value; }
        }

        /// <summary>
        /// 사용자에 의해 선택된 경로.
        /// </summary>
        public string SelectedPath
        {
            get { return m_ofdWrappedDialog.FileName; }
            set { m_ofdWrappedDialog.FileName = value; }
        }

        #endregion

        #region Methods
        ///
        /// <summary>
        /// Shows the dialog - 1
        /// </summary>
        /// <author>KyungChul Shin (kcshin@satreci.com)</author>
        /// <date>2014-04-21</date>
        //  begin-2014-08-25(kcshin) - Update the modification requests that is necessary to change the return type into DialogResult.
        /// <returns>DialogResult.OK if the user presses OK else DialogResult.Cancel</returns>    
        //public bool ShowDialog()
        //{
        //    return ShowDialog(IntPtr.Zero);
        //}

        /// <summary>
        /// 사용자에게 폴더 선택을 위해 제공되는 다이얼로그를 보임
        /// </summary>
        /// <author>KyungChul Shin (kcshin@satreci.com)</author>
        /// <date>2014-04-21</date>
        /// <returns>DialogResult.OK if the user presses OK else DialogResult.Cancel</returns>
        public DialogResult ShowDialog()
        {
            return ShowDialog(null);
        }
        //  end-2014-08-25(kcshin)

        ///
        /// <summary>
        /// ShowDialog() 와 동일하나 parent 컨트롤을 설정할 수 있음
        /// </summary>
        /// <author>KyungChul Shin (kcshin@satreci.com)</author>
        /// <date>2014-04-21</date>
        /// <param name="_hWndOwner">Handle of the control to be parent</param>
        /// <returns>DialogResult.OK if the user presses OK else DialogResult.Cancel</returns>
        //  begin-2014-08-25(kcshin) - Update the modification requests that is necessary to change the return type into DialogResult. 
        //public bool ShowDialog(IntPtr _hWndOwner)
        public DialogResult ShowDialog(System.Windows.Forms.IWin32Window _hWndOwner)
        //  end-2014-08-25(kcshin)
        {
            bool bReturn = false;

            if (Environment.OSVersion.Version.Major >= VISTA_OS_VERSION)
            {
                uint uiNum = 0;
                Type typeIFileDialog = ReflectionHelper.Instance.GetType(WINDOWS_FORM_NAMESPACE, WINDOWS_FORM_NAMESPACE, "FileDialogNative.IFileDialog");
                object oDialog = ReflectionHelper.Instance.Call(m_ofdWrappedDialog.GetType(), m_ofdWrappedDialog, "CreateVistaDialog");
                ReflectionHelper.Instance.Call(m_ofdWrappedDialog.GetType(), m_ofdWrappedDialog, "OnBeforeVistaDialog", oDialog);

                uint uiOptions = (uint)ReflectionHelper.Instance.Call(typeof(System.Windows.Forms.FileDialog), m_ofdWrappedDialog, "GetOptions");
                uiOptions |= (uint)ReflectionHelper.Instance.GetEnum(WINDOWS_FORM_NAMESPACE, WINDOWS_FORM_NAMESPACE, "FileDialogNative.FOS", "FOS_PICKFOLDERS");
                ReflectionHelper.Instance.Call(typeIFileDialog, oDialog, "SetOptions", uiOptions);

                object oFormObject = ReflectionHelper.Instance.New(WINDOWS_FORM_NAMESPACE, WINDOWS_FORM_NAMESPACE, "FileDialog.VistaDialogEvents", m_ofdWrappedDialog);
                object[] parameters = new object[] { oFormObject, uiNum };
                ReflectionHelper.Instance.Call(typeIFileDialog, oDialog, "Advise", parameters);

                uiNum = (uint)parameters[1];
                try
                {
                    int uiNum2 = -1;
                    if (_hWndOwner != null)
                        uiNum2 = (int)ReflectionHelper.Instance.Call(typeIFileDialog, oDialog, "Show", _hWndOwner.Handle);
                    else
                        uiNum2 = (int)ReflectionHelper.Instance.Call(typeIFileDialog, oDialog, "Show", IntPtr.Zero);
                    bReturn = 0 == uiNum2;
                }
                finally
                {
                    ReflectionHelper.Instance.Call(typeIFileDialog, oDialog, "Unadvise", uiNum);
                    GC.KeepAlive(oFormObject);
                }
            }
            else
            {
                var fbdDialog = new FolderBrowserDialog();
                fbdDialog.Description = this.Description;
                fbdDialog.SelectedPath = this.InitialDirectory;
                fbdDialog.ShowNewFolderButton = false;
                //  begin-2014-08-25(kcshin) - Update the modification requests that is necessary to change the return type into DialogResult.
                //                             Also, it is necessary to change the ShowDialog(IntPtr _hWndOwner) into ShowDialog(IWin32Window owner).
                //if (fbdDialog.ShowDialog(new WindowWrapper(_hWndOwner)) != DialogResult.OK) return false;
                if (fbdDialog.ShowDialog(_hWndOwner) != DialogResult.OK) return DialogResult.Cancel;
                //  end-2014-08-25(kcshin)
                m_ofdWrappedDialog.FileName = fbdDialog.SelectedPath;
                bReturn = true;
            }
            //  begin-2014-08-25(kcshin) - Update the modification requests that is necessary to change the return type into DialogResult.
            //return bReturn;
            if (!bReturn) return DialogResult.Cancel;
            return DialogResult.OK;
            //  end-2014-08-25(kcshin)
        }

        #endregion
    }

    public class ReflectionHelper
    {
        /// <summary>
        /// PropertyReflection을 위한 Singleton instance (priavte).
        /// </summary>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2015-06-15</date>
        private static IPropertyReflectionHelper m_propertyHelper = new PropertyReflectionHelper();

        /// <summary>
        /// PropertyReflection을 위한 Singleton property.
        /// </summary>
        /// <value>
        /// The property.
        /// </value>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2015-06-15</date>
        public static IPropertyReflectionHelper Property
        {
            get { return m_propertyHelper; }
        }

        /// <summary>
        /// InstanceReflection을 위한 Singleton instance (priavte).
        /// </summary>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2015-06-15</date>
        private static IInstanceReflectionHelper m_instanceHelper = new InstanceReflectionHelper();

        /// <summary>
        /// InstanceReflection을 위한 Singleton property.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2015-06-15</date>
        public static IInstanceReflectionHelper Instance
        {
            get { return m_instanceHelper; }
        }

        ///
        /// <summary>
        /// Default constructor (private).
        /// </summary>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2011-11-21</date>
        private ReflectionHelper()
        {
        }

        /// <summary>
        /// Reflection helper helps using a lambda expression for property.
        /// </summary>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2011-11-21</date>
        public interface IPropertyReflectionHelper
        {
            /// <summary>
            /// Gets the name.
            /// </summary>
            /// <typeparam name="T">Class type</typeparam>
            /// <param name="_expression">The _expression.</param>
            /// <returns>name</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            string GetName<T>(Expression<Func<T>> _expression);

            /// <summary>
            /// Gets the name.
            /// </summary>
            /// <typeparam name="T">In type</typeparam>
            /// <typeparam name="U">Out type</typeparam>
            /// <param name="_expression">The _expression.</param>
            /// <returns>name</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            string GetName<T, U>(Expression<Func<T, U>> _expression);

            /// <summary>
            /// Gets the property information.
            /// </summary>
            /// <typeparam name="T">In type</typeparam>
            /// <typeparam name="U">Out type</typeparam>
            /// <param name="_expression">The _expression.</param>
            /// <returnsPropertyInfo></returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            PropertyInfo GetPropertyInfo<T, U>(Expression<Func<T, U>> _expression);

            /// <summary>
            /// Gets the property information.
            /// </summary>
            /// <typeparam name="T">Class type</typeparam>
            /// <param name="_expression">The _expression.</param>
            /// <returns>PropertyInfo</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            PropertyInfo GetPropertyInfo<T>(Expression<Func<T>> _expression);

            /// <summary>
            /// Gets the type.
            /// </summary>
            /// <typeparam name="T">In type</typeparam>
            /// <typeparam name="U">Out type</typeparam>
            /// <param name="_expression">The _expression.</param>
            /// <returns>Type</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            Type GetType<T, U>(Expression<Func<T, U>> _expression);

            /// <summary>
            /// Gets the type.
            /// </summary>
            /// <typeparam name="T">Class type</typeparam>
            /// <param name="_expression">The _expression.</param>
            /// <returns>Type</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            Type GetType<T>(Expression<Func<T>> _expression);
        }

        /// <summary>
        /// Reflection helper helps using a lambda expression for property.
        /// </summary>
        /// <author>KyungChul Shin (kcshin@satreci.com)</author>
        /// <date>2014-09-21</date>
        public interface IInstanceReflectionHelper
        {
            /// <summary>
            /// News the specified _STR assembly name.
            /// </summary>
            /// <param name="_strAssemblyName">Name of the assembly.</param>
            /// <param name="_strNamespace">The namespace.</param>
            /// <param name="_strTypeName">Name of the type.</param>
            /// <param name="_aobjParameters">The object parameters.</param>
            /// <returns>object</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            object New(string _strAssemblyName, string _strNamespace, string _strTypeName, params object[] _aobjParameters);

            /// <summary>
            /// Calls the specified _type type.
            /// </summary>
            /// <param name="_typeType">The type.</param>
            /// <param name="_oObject">The object.</param>
            /// <param name="_strFunctionName">Name of the function.</param>
            /// <param name="_aobjParameters">The object parameters.</param>
            /// <returns>object</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            object Call(Type _typeType, object _oObject, string _strFunctionName, params object[] _aobjParameters);

            /// <summary>
            /// Gets the enum.
            /// </summary>
            /// <param name="_strAssemblyName">Name of the assembly.</param>
            /// <param name="_strNamespace">The namespace.</param>
            /// <param name="_strTypeName">Name of the type.</param>
            /// <param name="_strValueName">Name of the value.</param>
            /// <returns>object</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            object GetEnum(string _strAssemblyName, string _strNamespace, string _strTypeName, string _strValueName);

            /// <summary>
            /// Gets the type.
            /// </summary>
            /// <param name="_strAssemblyName">Name of the assembly.</param>
            /// <param name="_strNamespace">The namespace.</param>
            /// <param name="_strTypeName">Name of the type.</param>
            /// <returns>Type</returns>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2015-05-18</date>
            Type GetType(string _strAssemblyName, string _strNamespace, string _strTypeName);
        }

        #region ExpressionPropertyHelper
        /// <summary>
        /// Property helping get name, type and property info.
        /// </summary>
        /// <author>DongGeun Bang (dgb@satreci.com)</author>
        /// <date>2011-11-21</date>
        private class PropertyReflectionHelper : IPropertyReflectionHelper
        {
            ///
            /// <summary>
            /// Get a property name.
            /// </summary>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2011-11-21</date>
            /// <typeparam name="T"></typeparam>
            /// <typeparam name="U"></typeparam>
            /// <param name="_expression">Lambda expression</param>
            /// <returns></returns>
            public string GetName<T, U>(Expression<Func<T, U>> _expression)
            {
                try
                {
                    var member = _expression.Body as MemberExpression;
                    return member.Member.Name;
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to access member in express for getting name. {0}", _expression);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }

            ///
            /// <summary>
            /// Get a property name.
            /// </summary>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2011-11-21</date>
            /// <typeparam name="T"></typeparam>
            /// <param name="_expression">Lambda expression</param>
            /// <returns></returns>
            public string GetName<T>(Expression<Func<T>> _expression)
            {
                try
                {
                    var member = _expression.Body as MemberExpression;
                    return member.Member.Name;
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to access member in express for getting name. {0}", _expression);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }

            ///
            /// <summary>
            /// Get a property type.
            /// </summary>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2011-11-21</date>
            /// <typeparam name="T"></typeparam>
            /// <typeparam name="U"></typeparam>
            /// <param name="_expression">Lambda expression</param>
            /// <returns></returns>
            public Type GetType<T, U>(Expression<Func<T, U>> _expression)
            {
                try
                {
                    return _expression.Body.Type;
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to access member in express for getting type. {0}", _expression);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }

            ///
            /// <summary>
            /// Get a property type.
            /// </summary>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2011-11-21</date>
            /// <typeparam name="T"></typeparam>
            /// <param name="_expression">Lambda expression</param>
            /// <returns></returns>
            public Type GetType<T>(Expression<Func<T>> _expression)
            {
                try
                {
                    return _expression.Body.Type;
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to access member in express for getting type. {0}", _expression);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }

            ///
            /// <summary>
            /// Get a property info.
            /// </summary>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2011-11-21</date>
            /// <typeparam name="T"></typeparam>
            /// <typeparam name="U"></typeparam>
            /// <param name="_expression">Lambda expression</param>
            /// <returns></returns>
            public PropertyInfo GetPropertyInfo<T, U>(Expression<Func<T, U>> _expression)
            {
                try
                {
                    var member = _expression.Body as MemberExpression;
                    return member.Member as PropertyInfo;
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to access member in express for getting property info. {0}", _expression);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }

            ///
            /// <summary>
            /// Get a property info.
            /// </summary>
            /// <author>DongGeun Bang (dgb@satreci.com)</author>
            /// <date>2011-11-21</date>
            /// <typeparam name="T"></typeparam>
            /// <param name="_expression">Lambda expression</param>
            /// <returns></returns>
            public PropertyInfo GetPropertyInfo<T>(Expression<Func<T>> _expression)
            {
                try
                {
                    var member = _expression.Body as MemberExpression;
                    return member.Member as PropertyInfo;
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to access member in express for getting property info. {0}", _expression);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }
        }
        #endregion

        #region InstanceReflectionHelper
        private class InstanceReflectionHelper : IInstanceReflectionHelper
        {
            #region variables

            private string m_strNamespace;

            private Assembly m_asmAssemblyName;
            #endregion

            #region Constructors
            /// <summary>
            /// Constructor
            /// </summary>
            /// <author>KyungChul Shin (kcshin@satreci.com)</author>
            /// <date>2014-09-21</date>
            /// <param name="_strAssemblyName">A specific assembly name (used if the assembly name does not tie exactly with the namespace)</param>
            /// <param name="_strNamespace">The namespace containing types to be used</param>
            private bool Reflector(string _strAssemblyName, string _strNamespace)
            {
                m_strNamespace = _strNamespace;
                m_asmAssemblyName = null;

                try
                {
                    foreach (AssemblyName asmName in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
                    {
                        if (asmName.FullName.StartsWith(_strAssemblyName))
                        {
                            m_asmAssemblyName = Assembly.Load(asmName);
                            return true;
                        }
                    }
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to load initializer. Assembly: {0}. Namespace: {1}.", _strAssemblyName, _strNamespace);
                    throw new ArgumentException(strErrorMessage, _e);
                }

                return false;
            }
            #endregion

            #region Methods
            ///
            /// <summary>
            /// Return a type instance for a type '_strTypeName'.
            /// </summary>
            /// <author>KyungChul Shin (kcshin@satreci.com)</author>
            /// <date>2014-09-21</date>
            /// <param name="_strTypeName">The name of the type</param>
            /// <returns>A type instance</returns>
            public Type GetType(string _strAssemblyName, string _strNamespace, string _strTypeName)
            {
                if (!Reflector(_strAssemblyName, _strNamespace)) return null;
                Type typeReturn = null;

                try
                {
                    string[] astrNames = _strTypeName.Split('.');

                    if (astrNames.Length > 0)
                        typeReturn = m_asmAssemblyName.GetType(m_strNamespace + "." + astrNames[0]);

                    for (int iIndex = 1; iIndex < astrNames.Length; ++iIndex)
                    {
                        typeReturn = typeReturn.GetNestedType(astrNames[iIndex], BindingFlags.NonPublic);
                    }
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to get the type instance. Assembly: {0}. Namespace: {1}. Type name: {2}.", _strAssemblyName, _strNamespace, _strTypeName);
                    throw new ArgumentException(strErrorMessage, _e);
                }

                return typeReturn;
            }

            ///
            /// <summary>
            /// Create a new object of a named type passing along any params.
            /// </summary>
            /// <author>KyungChul Shin (kcshin@satreci.com)</author>
            /// <date>2014-09-21</date>
            /// <param name="_strTypeName">The name of the type to create</param>
            /// <param name="_aobjParameters">Any arameters</param>
            /// <returns>An instantiated type</returns>
            public object New(string _strAssemblyName, string _strNamespace, string _strTypeName, params object[] _aobjParameters)
            {
                if (!Reflector(_strAssemblyName, _strNamespace)) return null;
                Type typeObject = GetType(_strAssemblyName, _strNamespace, _strTypeName);

                try
                {
                    ConstructorInfo[] aciInfos = typeObject.GetConstructors();
                    foreach (ConstructorInfo ciInfo in aciInfos)
                    {
                        try
                        {
                            return ciInfo.Invoke(_aobjParameters);
                        }
                        catch { }
                    }
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to create the instance. Assembly: {0}. Namespace: {1}. Type name: {2}.", _strAssemblyName, _strNamespace, _strTypeName);
                    throw new ArgumentException(strErrorMessage, _e);
                }

                return null;
            }

            ///
            /// <summary>
            /// Calls method '_strFunctionName' on object '_oObject' which is of type 'type' passing parameters '_aobjParameters'
            /// </summary>
            /// <author>KyungChul Shin (kcshin@satreci.com)</author>
            /// <date>2014-09-21</date>
            /// <param name="_typeType">The type of _oObject</param>
            /// <param name="_oObject">The object on which to excute function '_strFunctionName'</param>
            /// <param name="_strFunctionName">The function to execute</param>
            /// <param name="_aobjParameters">The parameters to pass to function '_strFunctionName'</param>
            /// <returns>The result of the function invocation</returns>
            /// <exception cref="System.ArgumentException"></exception>
            public object Call(Type _typeType, object _oObject, string _strFunctionName, params object[] _aobjParameters)
            {
                try
                {
                    MethodInfo methInfo = methInfo = _typeType.GetMethod(_strFunctionName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    return methInfo.Invoke(_oObject, _aobjParameters);
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to call the instance. Type: {0}. Function Name: {1}.", _typeType.GetType(), _strFunctionName);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }


            ///
            /// <summary>
            /// Returns an enum value
            /// </summary>
            /// <author>KyungChul Shin (kcshin@satreci.com)</author>
            /// <date>2014-09-21</date>
            /// <param name="_strTypeName">The name of enum type</param>
            /// <param name="_strValueName">The name of the value</param>
            /// <returns>The enum value</returns>
            public object GetEnum(string _strAssemblyName, string _strNamespace, string _strTypeName, string _strValueName)
            {
                if (!Reflector(_strAssemblyName, _strNamespace)) return null;

                Type typeObject = GetType(_strAssemblyName, _strNamespace, _strTypeName);

                try
                {
                    FieldInfo fiInfo = typeObject.GetField(_strValueName);
                    return fiInfo.GetValue(null);
                }
                catch (Exception _e)
                {
                    string strErrorMessage = string.Format("Fail to get the enum value. Assembly: {0}. Namespace: {1}. Type name: {2}. Value name: {3}.", _strAssemblyName, _strNamespace, _strTypeName, _strValueName);
                    throw new ArgumentException(strErrorMessage, _e);
                }
            }
            #endregion
        }
        #endregion

    }
}
