﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PosAttDataGenerator
{
    public partial class PosAttDataGenerator : Form
    {
        public PosAttDataGenerator()
        {
            InitializeComponent();
            fc = new function();
            
            string inputFolder = Path.Combine(Directory.GetCurrentDirectory(), "Input");
            string outputFolder = Path.Combine(Directory.GetCurrentDirectory(), "Output");
            if (!Directory.Exists(inputFolder))
            {
                Directory.CreateDirectory(inputFolder);
            }

            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            time_textbox.Text = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");
        }

        public function fc;

        private void PosAttDataGenerator_Load(object sender, EventArgs e)
        {
            string now_path = System.Windows.Forms.Application.StartupPath;
            inputFileTextBox.Text = Path.Combine(Directory.GetCurrentDirectory(), "Input", "PosAttData_noise_SPC_001.json");
            outputFolderTextBox.Text = Path.Combine(Directory.GetCurrentDirectory(), "Output");

        }

        private void transfer_button_Click(object sender, EventArgs e)
        {

            if (!time_textbox.Text.Equals("") && !interval_textbox.Text.Equals(""))
            {

                string outputFileName = string.Format("PosAttData_noise_SPC_{0}.json", DateTime.Now.ToString("yyyyMMddHHmmss"));

                try
                {
                    if (fc.transfer_and_save(inputFileTextBox.Text, Path.Combine(outputFolderTextBox.Text, outputFileName), time_textbox.Text, interval_textbox.Text))
                    {
                        UpdateLog(string.Format("Success to geneate data file : {0}", outputFileName));
                    }
                    else
                    {
                        UpdateLog("Failed to geneated data file...");
                    }
                }
                catch(Exception ex)
                {
                    UpdateLog(string.Format("Exception : {0}, {1}", ex.Message, ex.StackTrace));
                }                
                
            }
            else
            {
                MessageBox.Show("Please input all time and step.");
            }
      
        }

        /// <summary>
        /// 입력 파일 선택
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectInputFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = Directory.GetCurrentDirectory();
                openFileDialog.Filter = "josn files (*.json)|*.json|All files (*.*)|*.*";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    inputFileTextBox.Text = openFileDialog.FileName;
                }
            }
        }

        /// <summary>
        /// Output 경로 선택
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectOutputFolder_Click(object sender, EventArgs e)
        {
            FolderSelectDialog dialog = new FolderSelectDialog();
            dialog.InitialDirectory = Directory.GetCurrentDirectory(); ;
            dialog.Description = "Select Output Folder";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                outputFolderTextBox.Text = dialog.SelectedPath;
            }
        }

        /// <summary>
        /// 로그전시
        /// </summary>
        /// <param name="time"></param>
        private void UpdateLog(string log)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(() => { UpdateLog(log); }));
            }
            else
            {
                logTextBox.AppendText(string.Format("[{0}] {1}\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), log));
            }
        }
    }
}
