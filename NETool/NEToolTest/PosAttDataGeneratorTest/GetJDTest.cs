﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PosAttDataGenerator;

namespace NEToolTest.PosAttDataGeneratorTest
{
    /// <summary>
    /// GetJDTest의 요약 설명
    /// </summary>
    [TestClass]
    public class GetJDTest
    {
        [TestMethod]
        public void GetJDtest()
        {
            function f = new function();
            int year = 2021;
            int month = 8;
            int day = 13;
            int hour = 11;
            int min = 40;
            double sec = 0.125;
            Assert.AreEqual(true, f.GetJD(year, month, day, hour, min, sec));
        }
        
    }
}
