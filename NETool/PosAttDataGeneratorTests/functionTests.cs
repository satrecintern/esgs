﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PosAttDataGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PosAttDataGenerator.Tests
{
    [TestClass()]
    public class functionTests
    {
        [TestMethod()]
        public void GetJDTest(int _iYear, int _iMonth, int _iDay, int _iHour, int _iMin, double _dSec)
        {

            double dFracday = _iHour / 24.0 + _iMin / 1440.0 + _dSec / 86400.0;

            double dJD = (double)(367 * _iYear - 7 * (_iYear + (_iMonth + 9) / 12) / 4 + 275 * _iMonth / 9 + _iDay) + 1721013.5;

            dJD += dFracday;

            Assert.AreEqual(true, dJD);
        }
    }
}